package org.codegrinders.treasure_hunter.controller;

import org.codegrinders.treasure_hunter.configuration.ResetDatabase;
import org.codegrinders.treasure_hunter.model.Marker;
import org.codegrinders.treasure_hunter.model.Puzzle;
import org.codegrinders.treasure_hunter.model.User;
import org.codegrinders.treasure_hunter.service.MarkerService;
import org.codegrinders.treasure_hunter.service.PuzzleService;
import org.codegrinders.treasure_hunter.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.List;

/**
 * This controller contains all the endpoints that have to do with the admin web front-end.
 */

@Controller
@RequestMapping("/")
public class AdminController {
    @Autowired
    PuzzleService puzzleService;

    @Autowired
    UserService userService;

    @Autowired
    MarkerService markerService;

    @Autowired
    ResetDatabase resetDatabase;

    @RequestMapping(value = {"/", "/home"})
    public String home() {
        return "home";
    }

    @RequestMapping(value = "/login")
    public String login() {
        return "login";
    }


    /**
     * MARKER ENDPOINTS
     */

    @GetMapping("/allUsers")
    public String showAllUsers(Model model) {
        model.addAttribute("users", userService.findAll());
        return "allUsers";
    }

    @GetMapping("/addUser")
    public String addUser(Model model) {
        model.addAttribute("user", new User());
        return "addUser";
    }

    @PostMapping(value = "/addUser", params = "action=save")
    public String addUser(User user, Model model) {
        model.addAttribute("user", userService.registerUser(user));
        return "redirect:/allUsers";
    }

    @PostMapping(value = "/addUser", params = "action=cancel")
    public String cancelAddUser() {
        return "redirect:/allUsers";
    }

    @GetMapping("/editUser/{id}")
    public String editUser(@PathVariable("id") String id, Model model) {
        User user = userService.findById(id)
                .orElseThrow(() -> new IllegalArgumentException("Invalid user Id:" + id));
        model.addAttribute("user", user);
        return "editUser";
    }

    @PostMapping("/editUser/{id}")
    public String editUser(@PathVariable("id") String id, User user, BindingResult result, Model model) {
        if (result.hasErrors()) {
            user.setId(id);
            return "editUser";
        }
        userService.editUser(user);
        model.addAttribute("users", userService.findAll());
        return "redirect:/allUsers";
    }

    @GetMapping("/deleteUser/{id}")
    public String deleteUser(@PathVariable("id") String id, Model model) {
        User user = userService.findById(id)
                .orElseThrow(() -> new IllegalArgumentException("Invalid user Id: " + id));
        userService.deleteUser(user.getId());
        model.addAttribute("users", userService.findAll());

        return "redirect:/allUsers";
    }


    /**
     * PUZZLE ENDPOINTS
     */

    @GetMapping("/allPuzzles")
    public String showAll(Model model) {
        model.addAttribute("puzzles", puzzleService.findAll());
        return "allPuzzles";
    }

    @GetMapping("/addPuzzle")
    public String getPuzzle(Model model) {
        model.addAttribute("puzzle", new Puzzle());
        return "addPuzzle";
    }

    @PostMapping(value = "/addPuzzle", params = "action=save")
    public String postPuzzle(Puzzle puzzle, Model model) {
        model.addAttribute("puzzle", puzzleService.addPuzzle(puzzle));
        return "redirect:/allPuzzles";
    }

    @PostMapping(value = "/addPuzzle", params = "action=cancel")
    public String cancelAddPuzzle() {
        return "redirect:/allPuzzles";
    }

    @GetMapping("/editPuzzle/{id}")
    public String showUpdateFormPuzzle(@PathVariable("id") String id, Model model) {
        Puzzle puzzle = puzzleService.findById(id)
                .orElseThrow(() -> new IllegalArgumentException("Invalid user Id:" + id));

        model.addAttribute("puzzle", puzzle);
        return "editPuzzle";
    }

    @PostMapping("/editPuzzle/{id}")
    public String editPuzzle(@PathVariable("id") String id, Puzzle puzzle, BindingResult result, Model model) {
        if (result.hasErrors()) {
            puzzle.setId(id);
            return "editPuzzle";
        }

        puzzleService.editPuzzle(puzzle);
        model.addAttribute("puzzles", puzzleService.findAll());
        return "redirect:/allPuzzles";
    }

    @GetMapping("/deletePuzzle/{id}")
    public String deletePuzzle(@PathVariable("id") String id, Model model) {
        Puzzle puzzle = puzzleService.findById(id)
                .orElseThrow(() -> new IllegalArgumentException("Invalid puzzle Id:" + id));
        puzzleService.deletePuzzle(puzzle.getId());
        model.addAttribute("puzzles", puzzleService.findAll());
        return "redirect:/allPuzzles";
    }


    /**
     * MARKER ENDPOINTS
     */

    @GetMapping("/allMarkers")
    public String showAllMarkers(Model model) {
        model.addAttribute("markers", markerService.findAll());
        return "allMarkers";
    }

    @GetMapping("/addMarker")
    public String addMarker(Model model) {
        model.addAttribute("marker", new Marker());
        return "addMarker";
    }

    @PostMapping(value = "/addMarker", params = "action=save")
    public String saveMarker(Marker marker, Model model) {
        model.addAttribute("marker", markerService.addMarker(marker));
        return "redirect:/allMarkers";
    }

    @PostMapping(value = "/addMarker", params = "action=cancel")
    public String cancelAddMarker() {
        return "redirect:/allMarkers";
    }

    @GetMapping("/editMarker/{id}")
    public String editMarker(@PathVariable("id") String id, Model model) {
        Marker marker = markerService.findById(id)
                .orElseThrow(() -> new IllegalArgumentException("Invalid marker Id: +" + id));
        model.addAttribute("marker", marker);
        return "editMarker";
    }

    @PostMapping("/editMarker/{id}")
    public String editMarker(@PathVariable("id") String id, Marker marker, BindingResult result, Model model) {
        if (result.hasErrors()) {
            marker.setId(id);
            return "editMarker";
        }
        markerService.editMarker(marker);
        model.addAttribute("markers", markerService.findAll());
        return "redirect:/allMarkers";
    }

    @GetMapping("/deleteMarker/{id}")
    public String deleteMarker(@PathVariable("id") String id, Model model) {
        Marker marker = markerService.findById(id)
                .orElseThrow(() -> new IllegalArgumentException("Invalid marker Id:" + id));
        markerService.deleteMarker(marker.getId());
        model.addAttribute("markers", markerService.findAll());
        return "redirect:/allMarkers";
    }

    /**
     * OTHER ENDPOINTS
     */

    @PostMapping(value = "/resetDB", params = "action=resetDB")
    public String resetDB(HttpServletRequest request) {
        resetDatabase.initializeDB();
        String referer = request.getHeader("Referer");
        if (referer.contains("edit")) {
            return "redirect:/home";
        } else {
            return "redirect:" + referer;
        }
    }

    @GetMapping("/search")
    public String search(@RequestParam(value = "term", required = false, defaultValue = "none") String term, Model model) {

        List<User> userResults = new ArrayList<>();
        List<Puzzle> puzzleResults = new ArrayList<>();
        List<Marker> markerResults = new ArrayList<>();

        userResults.addAll(userService.findUserById(term));
        userResults.addAll(userService.findAllUsersByEmailLike(term));
        userResults.addAll(userService.findAllUsersByUsernameLike(term));

        puzzleResults.addAll(puzzleService.findPuzzleById(term));
        puzzleResults.addAll(puzzleService.findAllByQuestionLike(term));
        puzzleResults.addAll(puzzleService.findAllByAnswerLike(term));

        markerResults.addAll(markerService.findMarkerById(term));
        markerResults.addAll(markerService.findAllByAreaLike(term));
        markerResults.addAll(markerService.findAllByHintLike(term));

        model.addAttribute("users", userResults);
        model.addAttribute("puzzles", puzzleResults);
        model.addAttribute("markers", markerResults);

        return "search";
    }

    @GetMapping("/autocomplete")
    @ResponseBody
    public List<Puzzle> autoComplete(@RequestParam(value = "term", required = false, defaultValue = "") String term) {
        return puzzleService.findByQuery(term);
    }

    @GetMapping("/home")
    public String statistics(Model model) {
        model.addAttribute("countUsers", userService.countUsers());
        model.addAttribute("countPuzzles", puzzleService.countPuzzles());
        model.addAttribute("countMarkers", markerService.countMarkers());
        return "home";
    }

}