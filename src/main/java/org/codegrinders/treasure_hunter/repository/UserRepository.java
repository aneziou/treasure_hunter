package org.codegrinders.treasure_hunter.repository;

import org.codegrinders.treasure_hunter.model.User;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;


@Repository("userRepository")
public interface UserRepository extends MongoRepository<User, String> {

    boolean existsByUsername(String username);

    User findUserByUsername(String username);

    User findUserByEmail(String email);

    List<User> findUserById(String id);

    List<User> findAllByUsernameLike(String username);

    List<User> findAllByEmailLike(String email);

    List<User> findAllByOrderByPointsDesc();

    @Query(value = "{}", count = true)
    String countUsersByUsername();
}