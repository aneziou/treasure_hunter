package org.codegrinders.treasure_hunter.repository;

import org.codegrinders.treasure_hunter.model.Marker;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;

import java.util.List;

public interface MarkerRepository extends MongoRepository<Marker, String> {

    List<Marker> findMarkerById(String id);

    List<Marker> findAllByAreaLike(String area);

    List<Marker> findAllByHintLike(String hint);

    @Query(value = "{}", count = true)
    String countMarkerById();

}
