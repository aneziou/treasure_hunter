package org.codegrinders.treasure_hunter.repository;

import org.codegrinders.treasure_hunter.model.Puzzle;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;


@Repository
public interface PuzzleRepository extends MongoRepository<Puzzle, String> {

    List<Puzzle> getPuzzleByQuestion(String question);

    List<Puzzle> findByQuestion(String question);

    List<Puzzle> findAllByQuestion(String question);

    List<Puzzle> findAllByQuestionLike(String question);

    List<Puzzle> findAllByAnswerLike(String answer);

    List<Puzzle> findPuzzleById(String id);

    @Query(value = "{}", count = true)
    String countPuzzleById();

    @Query("{'question': {$regex: ?0 }})")
    List<Puzzle> findByQuery(String term);

}
