package org.codegrinders.treasure_hunter;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.data.mongodb.repository.config.EnableMongoRepositories;


@SpringBootApplication
@EnableMongoRepositories
public class TreasureHunterApplication {

    public static void main(String[] args) {
        SpringApplication.run(TreasureHunterApplication.class, args);
    }

}