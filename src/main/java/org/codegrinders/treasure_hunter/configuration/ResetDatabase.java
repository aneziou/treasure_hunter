package org.codegrinders.treasure_hunter.configuration;

import org.codegrinders.treasure_hunter.model.Marker;
import org.codegrinders.treasure_hunter.model.Puzzle;
import org.codegrinders.treasure_hunter.model.User;
import org.codegrinders.treasure_hunter.repository.MarkerRepository;
import org.codegrinders.treasure_hunter.repository.PuzzleRepository;
import org.codegrinders.treasure_hunter.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.time.LocalDateTime;

/**
 * As the name suggests this class reset the database and import some dummy default values.
 */

@Component
public class ResetDatabase {

    @Autowired
    private PuzzleRepository puzzleRepository;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private MarkerRepository markerRepository;

    public void initializeDB() {
        puzzleRepository.deleteAll();
        userRepository.deleteAll();
        markerRepository.deleteAll();

        puzzleRepository.save(new Puzzle("You had five apples but you ate one. How many apples are left? (4)", "4", 10));
        puzzleRepository.save(new Puzzle("Does the donkey fly? (no)", "no", 5));
        puzzleRepository.save(new Puzzle("Are pineapples and oranges vegetables? (no)", "no", 10));
        puzzleRepository.save(new Puzzle("Who was the first human in space? (Yuri)", "Yuri", 50));
        puzzleRepository.save(new Puzzle("Is it possible to climb a ladder to the moon? (no)", "no", 3));
        puzzleRepository.save(new Puzzle("Which season is the coldest? (winter)", "winter", 49));

        userRepository.save(new User("sadman123@codegrinders.org", "makis", "Asdf12!@", 0, LocalDateTime.now()));
        userRepository.save(new User("ouaou99@codegrinders.org", "sakis", "Asdf12!@", 0, LocalDateTime.now()));
        userRepository.save(new User("monochrome@codegrinders.org", "takis", "Asdf12!@", 0, LocalDateTime.now()));

        markerRepository.save(new Marker(41.07639384506259, 23.55436861607741, "Library", "easy", puzzleRepository.findAll().get(0).getId(), true, "There is a puzzle close to the Canteen"));
        markerRepository.save(new Marker(41.074579055248044, 23.553908368320705, "Canteen", "medium", puzzleRepository.findAll().get(1).getId(), true, "There is a puzzle close to the Management Building"));
        markerRepository.save(new Marker(41.07641471735913, 23.553189582884215, "Management Building", "hard", puzzleRepository.findAll().get(2).getId(), true, "There is a puzzle close to the Student Club"));
        markerRepository.save(new Marker(41.07559847572417, 23.550664485427845, "Student Club", "easy", puzzleRepository.findAll().get(3).getId(), true, "There is a puzzle close to the Theatre"));
        markerRepository.save(new Marker(41.07416585770809, 23.554070023473408, "Theatre", "medium", puzzleRepository.findAll().get(4).getId(), true, "There is a puzzle close to the Computer Science Department"));
        markerRepository.save(new Marker(41.07458492957822, 23.555268268790538, "Computer Science Department", "hard", puzzleRepository.findAll().get(5).getId(), true, "There is a puzzle close to the Library"));
    }
}
