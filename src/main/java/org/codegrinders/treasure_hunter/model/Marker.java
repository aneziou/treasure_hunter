package org.codegrinders.treasure_hunter.model;

import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.PersistenceCreator;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "Marker")
public class Marker {

    @Id
    private String id;
    private double latitude;
    private double longitude;
    private String area;
    private String difficulty;
    private String puzzleId;
    private boolean visibility;
    private String hint;

    @PersistenceCreator
    public Marker(String id, double latitude, double longitude, String area, String difficulty, String puzzleId, boolean visibility, String hint) {
        this.id = id;
        this.latitude = latitude;
        this.longitude = longitude;
        this.area = area;
        this.difficulty = difficulty;
        this.puzzleId = puzzleId;
        this.visibility = visibility;
        this.hint = hint;
    }

    public Marker(double latitude, double longitude, String area, String difficulty, String puzzleId, boolean visibility, String hint) {
        this.latitude = latitude;
        this.longitude = longitude;
        this.area = area;
        this.difficulty = difficulty;
        this.puzzleId = puzzleId;
        this.visibility = visibility;
        this.hint = hint;
    }

    public Marker(double latitude, double longitude, String area, String difficulty, String puzzleId, boolean visibility) {
        this.latitude = latitude;
        this.longitude = longitude;
        this.area = area;
        this.difficulty = difficulty;
        this.puzzleId = puzzleId;
        this.visibility = visibility;
        this.hint = null;
    }

    public Marker(String hint) {
        this.hint = hint;
    }

    public Marker() {
    }

    public String getId() {
        return id;
    }

    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }

    public String getArea() {
        return area;
    }

    public void setArea(String area) {
        this.area = area;
    }

    public String getDifficulty() {
        return difficulty;
    }

    public void setDifficulty(String difficulty) {
        this.difficulty = difficulty;
    }

    public String getPuzzleId() {
        return puzzleId;
    }

    public void setPuzzleId(String puzzleId) {
        this.puzzleId = puzzleId;
    }

    public boolean getVisibility() {
        return visibility;
    }

    public void setVisibility(boolean visibility) {
        this.visibility = visibility;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getHint() {
        return hint;
    }

    @Override
    public String toString() {
        return "Marker{" +
                "id='" + id + '\'' +
                ", latitude=" + latitude +
                ", longitude=" + longitude +
                ", area='" + area + '\'' +
                ", difficulty='" + difficulty + '\'' +
                ", puzzleId='" + puzzleId + '\'' +
                ", visibility=" + visibility +
                ", hint='" + hint + '\'' +
                '}';
    }
}
