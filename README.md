<a name="readme-top"></a>

<div align="center" style="width: 100%;">

[![Pipeline_Status](https://gitlab.com/aneziou/treasure_hunter/badges/master/pipeline.svg)](https://gitlab.com/aneziou/treasure_hunter/-/commits/master)
[![Current_Version](https://img.shields.io/badge/version-0.2.2-blue.svg)](https://gitlab.com/aneziou/treasure_hunter/-/releases)
[![License](https://img.shields.io/badge/license-GPLv3-blue.svg)](LICENSE)
[![Team](https://img.shields.io/badge/Meet_the-Team-blueviolet.svg)](https://gitlab.com/aneziou/treasure_hunter/-/project_members)
[![Demo](https://img.shields.io/badge/latest_demo-available-brightgreen.svg)](https://treasure-hunter-latest.herokuapp.com/)
[![Old_Demo](https://img.shields.io/badge/v0.1.0_demo-available-red.svg)](https://treasure-hunter-codegrinders.herokuapp.com/)

[![THMobile](https://img.shields.io/badge/Treasure_Hunter_Mobile_Repository-Click_here-yellow.svg)](https://gitlab.com/aneziou/treasure_hunter_mobile)

</div></br>

<div align="center">

![](/doc/readme/logo.svg)

  <h1 align="center">Treasure Hunter</h1>

</div>

**<p align="center">A platform to create treasure hunt games, utilizing new technologies,<br>to uplift this old good game into the 21st century and beyond!</p>**
<br>

| ![](doc/readme/th-landing-page.jpg)  | ![](doc/readme/th-search.png)  |
|:------------------------------------:|:------------------------------:|

| ![](doc/readme/th-homepage.png) | ![](doc/readme/th-new-marker.png)  |
|:-------------------------------:|:----------------------------------:|

## Contents

<h3>
<b>

- <a href="#about">About</a>
  - <a href="#feature-list">Feature List</a>
  - <a href="#architecture-model">Architecture Model</a>
- <a href="#demo">Demo</a>
  -  <a href="#online">Online</a>
  -  <a href="#gif">Gif</a>
- <a href="#requirements">Requirements</a>
  - <a href="#optional">Optional</a>
- <a href="#installation">Installation</a>
  - <a href="#methods">Methods</a>
- <a href="#tools">Tools</a>
- <a href="#sources">Sources</a>
- <a href="#licence">Licence</a>

</b>
</h3>

<!-- ABOUT -->
# About
> **Notice**:
>> This was a team project for the "Software Technologies" lesson by <a href="https://gr.linkedin.com/in/nikospetalidis">Dr. Nikolaos Petalidis</a> at the 7th semester of the **I**nternational **H**ellenic **U**niversity, Serres Campus (former TEI of Central Macedonia) and the Computer Engineering department. You can see the team members **<a href="https://gitlab.com/aneziou/treasure_hunter/-/project_members">here</a>**!

The **Treasure Hunter** project consists of two repositories, **<a href="https://gitlab.com/aneziou/treasure_hunter">Treasure Hunter</a>** and **<a href="https://gitlab.com/aneziou/treasure_hunter_mobile">Treasure Hunter Mobile</a>**:
- **<a href="https://gitlab.com/aneziou/treasure_hunter">Treasure Hunter</a>** contains the source code for the **backend** and the **admin frontend**
- **<a href="https://gitlab.com/aneziou/treasure_hunter_mobile">Treasure Hunter Mobile</a>** contains the source code for the **player frontend**.

The **backend** and **Treasure Hunter Mobile** are written in `Java`. The **admin frontend** is a **web app** and can be used in a browser from any capable device. The **Player Frontend** on the other hand, can only be used in an Android OS environment. For more details, visit the other repository <a href="https://gitlab.com/aneziou/treasure_hunter_mobile">here</a>.

Some of the technologies used for the **backend** and the **admin frontend** are:
- `HTML`
- `CSS`
- `JavaScript`
- `Java`
- `Spring Boot`
- `MongoDB`
- `Bootstrap 5`
- `Thymeleaf`
- `Google Maps`

<br>

## Feature List

A list of **major** features, implemented and pending:

- [X] **API**
- [X] **Login**
- [x] **Database Connection**
- [X] **CRUD Functions**
- [X] **Google Maps**
- [X] **Admin Frontend**
- [ ] Security
- [ ] Testing
- [ ] Production Ready

<br>

## Architecture Model

The project is based on the **MVC** (**M**odel **V**iew **C**ontroller) architectural pattern.

Below, you can see a model of the project architecture.

<div align="center">

  <img style="" src="doc/readme/th_arch.png">

</div>

<hr>
<div align="left">
  <b><a href="#about">About</a> - <a href="#demo">Demo</a> - <a href="#requirements">Requirements</a> - <a href="#installation">Installation</a> - <a href="#tools">Tools</a> - <a href="#sources">Sources</a> - <a href="#licence">Licence</a></b>
</div>

<div align="right">
    (<a href="#readme-top"><b>back to top 🠉</b></a>)
</div>
<hr>
<br>


<!-- DEMO -->
# Demo

There are two versions of the project online, hosted on `Heroku`. The `v0.1.0 (first release)` and the `v0.2.2(latest)`.

The `Heroku Dynos` can go to sleep if inactive for 30 minutes, so give it a few seconds to load initially.

In case they are down, you can watch the `gif` files below to get an idea.

<br>

## Online

Version 0.2.2 (latest):
> <a href="https://treasure-hunter-latest.herokuapp.com/">https://treasure-hunter-latest.herokuapp.com/</a>


Version 0.1.0(first release):
> <a href="https://treasure-hunter-codegrinders.herokuapp.com/">https://treasure-hunter-codegrinders.herokuapp.com/</a>

<br>

## Gif

<div align="center">

### Version 0.2.2

| <img style="" src="doc/readme/project.gif"> |
|:-------------------------------------------:|

### Version 0.1.0 + Player Frontend (Android)

| <img style="" src="doc/readme/thm-video.gif"> |
|:---------------------------------------------:|

</div>

<hr>
<div align="left">
  <b><a href="#about">About</a> - <a href="#demo">Demo</a> - <a href="#requirements">Requirements</a> - <a href="#installation">Installation</a> - <a href="#tools">Tools</a> - <a href="#sources">Sources</a> - <a href="#licence">Licence</a></b>
</div>

<div align="right">
    (<a href="#readme-top"><b>back to top 🠉</b></a>)
</div>
<hr>
<br>


<!-- REQUIREMENTS -->
# Requirements

- <a href="https://www.oracle.com/eg/java/technologies/javase/javase8u211-later-archive-downloads.html"><b>Java 8 JDK</b></a>
  - It should work up to Java 14, but for later versions, you might need to tweak the code.

- <a href="https://maven.apache.org/download.cgi"><b>Maven</b></a>
  - Will build the project and fetch dependencies for you (it comes bundled with IntelliJ IDEA).


## Optional

- <a href="https://www.mongodb.com/try/download/community"><b>MongoDB</b></a>
  - You will need to download the MongoDB Community Server if you want to host a local database instead of a cloud service.

<hr>
<div align="left">
  <b><a href="#about">About</a> - <a href="#demo">Demo</a> - <a href="#requirements">Requirements</a> - <a href="#installation">Installation</a> - <a href="#tools">Tools</a> - <a href="#sources">Sources</a> - <a href="#licence">Licence</a></b>
</div>

<div align="right">
    (<a href="#readme-top"><b>back to top 🠉</b></a>)
</div>
<hr>
<br>


<!-- INSTALLATION -->
# Installation

There are different ways for you to install and run this program. Below, I will provide you with two alternatives.

- [1] The first method lets you download and try the project as is.
  > If the online <b><a href="https://treasure-hunter-latest.herokuapp.com/">demo</a></b> on Heroku is down, you can try this method and run the server locally. Using this method will still require the MongoDB Atlas database to be up and running; otherwise, the application will crash. In that case, you must use the second method.

- [2] The second method guides you through various steps so you can alter the program and use your database and server (either local or in the cloud).

<br>

## Methods


- [1]
  - Download the latest <a href="https://gitlab.com/aneziou/treasure_hunter/-/releases">release</a> artifacts from <b>Assets/other -> release</b>.
  - Unzip the file to a folder of your choosing.
  - Open the subfolder with the name <b>target</b>.
  - Run the file `treasure_hunter-0.2.2.jar`.
    - You can run it through a CLI with the following command to see if something is wrong.<br>`java -jar treasure_hunter-0.2.2.jar`
  - The application will start a process in your system.
  - The process name should be `Java (TM) Platform SE binary`, in case you want to kill it when you are done.
  - The `Admin Frontend` should now be visible if you open a browser and visit http://localhost:8080/.
  - PLAY!

<hr>

# <div align="center"><b>or</b></div>

- [2]
  - Download and install the <a href="https://www.oracle.com/eg/java/technologies/javase/javase8u211-later-archive-downloads.html"><b>Java 8 JDK</b></a>.
  - Download and install <a href="https://www.jetbrains.com/idea/download/"><b>JetBrains IntelliJ IDEA Community Edition</b></a> (or another IDE).
  - Download the <a href="https://gitlab.com/aneziou/treasure_hunter"><b>source code</b></a> or open <b>IntelliJ IDEA</b>, in the opening window press "<b>Get from VCS</b>".
  - Run `Maven` update to get dependencies, in case it didn't run automatically.
  - In the file <b>env.properties</b>, provide a `<database name>` for "MONGODB_NAME" and a `<database URI>` for "MONGODB_URI".
  - Build and run the project.
  - The `admin frontend` should now be visible if you open a browser and visit http://localhost:8080/.
  - PLAY!

<hr>
<div align="left">
  <b><a href="#about">About</a> - <a href="#demo">Demo</a> - <a href="#requirements">Requirements</a> - <a href="#installation">Installation</a> - <a href="#tools">Tools</a> - <a href="#sources">Sources</a> - <a href="#licence">Licence</a></b>
</div>

<div align="right">
    (<a href="#readme-top"><b>back to top 🠉</b></a>)
</div>
<hr>
<br>


<!-- TOOLS -->
# Tools

- **IDE**: `Jetbrains IntelliJ Idea Ultimate (student)` to write, build, debug, and test the source code.
- **DB**: `MongoDB Atlas` to host the database on the cloud.
- **Server**: `HEROKU` to host the Java application on the cloud.
- **Drawings**: `Microsoft Visio 2016`, to create the Architecture Model drawing.
- **Gifs**: `ShareX` to create the gif demo animations.


<hr>
<div align="left">
  <b><a href="#about">About</a> - <a href="#demo">Demo</a> - <a href="#requirements">Requirements</a> - <a href="#installation">Installation</a> - <a href="#tools">Tools</a> - <a href="#sources">Sources</a> - <a href="#licence">Licence</a></b>
</div>

<div align="right">
    (<a href="#readme-top"><b>back to top 🠉</b></a>)
</div>
<hr>
<br>


<!-- SOURCES -->
# Sources

Sources I used materials to create the project and readme.

* [Shields.io](https://shields.io/)
* [NameCheap Logo Maker](https://www.namecheap.com/logo-maker/)
* [Best-README-Template](https://github.com/othneildrew/Best-README-Template/)

<hr>
<div align="left">
  <b><a href="#about">About</a> - <a href="#demo">Demo</a> - <a href="#requirements">Requirements</a> - <a href="#installation">Installation</a> - <a href="#tools">Tools</a> - <a href="#sources">Sources</a> - <a href="#licence">Licence</a></b>
</div>

<div align="right">
    (<a href="#readme-top"><b>back to top 🠉</b></a>)
</div>
<hr>
<br>


<!-- LICENSE -->
# License

The license used for the project is [GPLv3](LICENSE).

<hr>
<div align="left">
  <b><a href="#about">About</a> - <a href="#demo">Demo</a> - <a href="#requirements">Requirements</a> - <a href="#installation">Installation</a> - <a href="#tools">Tools</a> - <a href="#sources">Sources</a> - <a href="#licence">Licence</a></b>
</div>

<div align="right">
    (<a href="#readme-top"><b>back to top 🠉</b></a>)
</div>
<hr>
<br>
